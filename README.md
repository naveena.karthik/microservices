# Adidas Subscription System
A secure spring boot microservice application that can be invoked by an external API gateway for subscribtion purposes. This application connects to downstream secured service to send emails and also connects to Postgres database for storage/retrieval of subscription information.

## Architecture
The system comprises of 5 microservices:

- A sping cloud gateway microservice acts as a public facing service, which gets invoked by the front-end applications. This microservice recieves a Bearer Authorization token (for simplicity sake, the system that generates the JWS token has not been designed) 
- The API gateway microservice forwards the JWS token to authorization microservice for validation.
- Only if the token is found to be valid, the request is forwarded to the subscription microservice thereby making it secure and impossible to be invoked directly by external clients.
- The subscription microservice invokes the email microservice to send emails.
- We have a Eureka server microservice that registers all the above microservices and provides LoadBalancing capabilities


## Design Diagram
A copy of the architecure diagram can be found in the root folder - Architecture-Diagram.png

## Libraries and Frameworks used

- Spring Boot 
    -An open source framework, easily configurable and contains production-ready features thereby making it ideal to be used in Microservice architecture

- Spring Data JPA 
    -A module which makes it easy to implement the data access CRUD operations.

- spring-cloud-starter-netflix-eureka-server
    -A library that helps a microservice to act like a service registry. A service registry holds information about all the applications running on each port and IP address.

- spring-cloud-starter-netflix-eureka-client
    -A library that helps register the microservice with Eureka server (service registry) so that it can be identified by peers using only its spring application name.

- spring-boot-starter-actuator
    -A library that helps us to monitor and collect metrics from a running microservice.

- spring-cloud-starter-circuitbreaker-resilience4j
    -A fault tolerance library that helps to monitor failing downstream microservice invocations and provide fallback mechanisms based on configurable parameters.

- spring-boot-starter-aop
    -Circuit Breaker uses AOP annotations to provide cross cutting concerns.

- springdoc-openapi-ui
    -A library that helps automate the generation of API documentation

- spring-cloud-starter-gateway
    -A non blocking API library that helps route client requests to downstream microservices

- spring-boot-starter-webflux
    -A library providing support for non-blocking reactive web applications. Spring Cloud Gateway uses Spring   webflux. 

- spring-cloud-starter-circuitbreaker-reactor-resilience4j
    -A reactive fault tolerance library

- junit
    -A simple framework to write unit tests

- Mockito
    -A simple framework that helps to mock interfaces for effective unit testing.

## Tools used
- Maven
- Docker

## Installation

- Git clone main branch on project https://gitlab.com/naveena.karthik/microservices
- Once downloaded, navigate to root directory and run the command
  **docker compose up --build**
- Once all containers are up, run the below Postman commands to test various endpoints:

```
  subscribe::POST
  http://localhost:8080/subscription/subscribe
  Headers:
   Content-Type: application/json
   Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJuYXZlZW5hIiwiaWF0IjoxNTE2MjM5MDIyfQ.3DPadlm6BBxX67DwfvgSx-Gsd9ckkWZt2ksRUuLnvjA
   Body:
   { 
    "email":"email@abc.com",
    "firstName":"firstName",
    "gender":"male",
    "newsletterId":6,
    "consentFlag":true,
    "dateOfBirth":"12/01/1986"
   }
```
```
  get_subscription::GET
  http://localhost:8080/subscription/get_subscription
  Headers:
   Content-Type: application/json
   Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJuYXZlZW5hIiwiaWF0IjoxNTE2MjM5MDIyfQ.3DPadlm6BBxX67DwfvgSx-Gsd9ckkWZt2ksRUuLnvjA
   Body:
   { 
    "email":"email@abc.com",
    "firstName":"firstName",
    "gender":"male",
    "newsletterId":6,
    "consentFlag":true,
    "dateOfBirth":"12/01/1986"
   }
```
```
  get_all_subscriptions::GET
  http://localhost:8080/subscription/get_all_subscriptions
  Headers:
   Content-Type: application/json
   Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJuYXZlZW5hIiwiaWF0IjoxNTE2MjM5MDIyfQ.3DPadlm6BBxX67DwfvgSx-Gsd9ckkWZt2ksRUuLnvjA
```
```
  cancel_subscription::DELETE
  http://localhost:8080/subscription/cancel_subscription/{id}
  Headers:
   Content-Type: application/json
   Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJuYXZlZW5hIiwiaWF0IjoxNTE2MjM5MDIyfQ.3DPadlm6BBxX67DwfvgSx-Gsd9ckkWZt2ksRUuLnvjA
```

```
Standard Response:
**HTTP STATUS: 200**
Response Body:
{
    "subscriptions": [
        {
            "id": 2,
            "email": "email702",
            "firstName": "test702",
            "gender": "male",
            "dateOfBirth": "1986-01-12",
            "newsletterId": 6,
            "consentFlag": true
        }
    ],
    "emailResponse": "Email sent successfully"
}
```

## OpenAPI 3
OpenAPI swagger documentation can be found at:
`http://localhost:8081/swagger-ui.html`

## Actuator metrics
Actuator health and metrics can be checked out at:
- `http://localhost:8086/actuator`
- `http://localhost:8086/actuator/metrics`


## CI/CD Pipeline
.gitlab-ci.yml located at the root directory contains the CI/CD configuration for this project.
This file defines the project's pipeline in stages and also contains environment variables.

Each push of the code to the feature branch triggers the build and deployment.
The pipeline process runs, building the images for each of the microservices and pushing them to the registry.
They are then deployed to K8 pods in Kubernetes cluster.

The pipeline uses a container image of Docker to build the docker images (dind).


