package com.adidas.emailservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailServiceController {
    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
    public ResponseEntity<String> sendEmail(@RequestHeader("auth-user-id") String userId) {
        return ResponseEntity.status(HttpStatus.OK).body("Email sent successfully");
    }
}
