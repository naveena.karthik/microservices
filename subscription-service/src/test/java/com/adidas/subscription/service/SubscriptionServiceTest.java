package com.adidas.subscription.service;

import com.adidas.subscription.exception.SubscriptionAlreadyExistsException;
import com.adidas.subscription.exception.SubscriptionNotFoundException;
import com.adidas.subscription.exception.SubscriptionServiceException;
import com.adidas.subscription.model.Gender;
import com.adidas.subscription.model.Subscription;
import com.adidas.subscription.model.SubscriptionResponse;
import com.adidas.subscription.repository.SubscriptionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.postgresql.util.PSQLException;
import org.postgresql.util.PSQLState;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.QueryTimeoutException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

public class SubscriptionServiceTest {
    private final SubscriptionRepository subscriptionRepository = mock(SubscriptionRepository.class);
    private final InvokeEmailService invokeEmailService = mock(InvokeEmailService.class);
    private final SubscriptionService service = new SubscriptionService(
            subscriptionRepository,
            invokeEmailService);
    private final LocalDate dob = LocalDate.parse("12/01/1985", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    private final Subscription subscription = new Subscription();
    private final String exception_message = "Unique Constraint Violation";

    @BeforeEach
    public void setup() {
        subscription.setId(1L);
        subscription.setEmail("email");
        subscription.setFirstName("test");
        subscription.setGender(Gender.male);
        subscription.setNewsletterId(2);
        subscription.setDateOfBirth(dob);
        subscription.setId(1L);
    }

    @Test
    void createSubscriptionTest_valid() {
        when(subscriptionRepository.save(any(Subscription.class))).thenReturn(subscription);
        when(invokeEmailService.invoke()).thenReturn("dummyResponse");
        SubscriptionResponse response = service.createSubscription(subscription);
        then(subscriptionRepository).should(times(1)).save(subscription);
        assertThat(response.getSubscriptions().size(), is(1));
        assertThat(response.getSubscriptions().get(0).getId(), is(1L));
        assertThat(response.getEmailResponse(), is("dummyResponse"));
    }

    @Test
    void createSubscriptionTest_uniqueViolation() {
        when(subscriptionRepository.save(any(Subscription.class))).thenThrow(createMockException());
        assertThrows(SubscriptionAlreadyExistsException.class, () -> service.createSubscription(subscription));
    }

    @Test
    void createSubscriptionTest_generalException() {
        when(subscriptionRepository.save(any(Subscription.class))).thenThrow(new QueryTimeoutException(""));
        assertThrows(SubscriptionServiceException.class, () -> service.createSubscription(subscription));
    }

    @Test
    void cancelSubscriptionTest() {
        doNothing().when(subscriptionRepository).deleteByEmailAndNewsletterIdAndDateOfBirth("email", 2, dob);
        service.cancelSubscription(1L);
        then(subscriptionRepository).should(times(1)).deleteById(1L);
    }

    @Test
    void getSubscriptionTest_valid() {
        when(subscriptionRepository.findByEmailAndNewsletterIdAndDateOfBirth("email", 2, dob)).thenReturn(Optional.of(subscription));
        SubscriptionResponse response = service.getSubscription(subscription);
        then(subscriptionRepository).should(times(1)).findByEmailAndNewsletterIdAndDateOfBirth("email", 2, dob);
        assertThat(response.getSubscriptions().size(), is(1));
        assertThat(response.getSubscriptions().get(0).getId(), is(1L));
    }

    @Test
    void getSubscriptionTest_notFound() {
        when(subscriptionRepository.findByEmailAndNewsletterIdAndDateOfBirth("email", 2, dob)).thenThrow(new SubscriptionNotFoundException());
        assertThrows(SubscriptionNotFoundException.class, () -> service.getSubscription(subscription));
    }

    @Test
    void getAllSubscriptionsTest() {
        List<Subscription> subscriptionList = Arrays.asList(subscription);
        when(subscriptionRepository.findAll()).thenReturn(subscriptionList);
        SubscriptionResponse response = service.getAllSubscriptions();
        then(subscriptionRepository).should(times(1)).findAll();
        assertThat(response.getSubscriptions().size(), is(1));
        assertThat(response.getSubscriptions().get(0).getId(), is(1L));
    }

    private DataIntegrityViolationException createMockException() {
        PSQLException psqlException = new PSQLException(exception_message, PSQLState.UNIQUE_VIOLATION);
        DataIntegrityViolationException exception = new DataIntegrityViolationException(exception_message, psqlException);
        return exception;
    }
}

