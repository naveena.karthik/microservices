package com.adidas.subscription.controller;

import com.adidas.subscription.exception.ApiError;
import com.adidas.subscription.exception.SubscriptionNotFoundException;
import com.adidas.subscription.model.Gender;
import com.adidas.subscription.model.Subscription;
import com.adidas.subscription.model.SubscriptionRequest;
import com.adidas.subscription.model.SubscriptionResponse;
import com.adidas.subscription.service.SubscriptionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class SubscriptionControllerTest {

	@Autowired
	private ApplicationContext context;
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private SubscriptionService subscriptionService;

	private final LocalDate dob = LocalDate.parse("12/01/1985", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	private ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	public void setup() {
		objectMapper.registerModule(new JavaTimeModule());
		openMocks(this);
		setupService();
	}

	@AfterEach
	public void tearDown() {
		Mockito.reset();
	}

	private void setupService() {
		SubscriptionResponse response = new SubscriptionResponse();
		Subscription subscription = new Subscription();
		subscription.setId(1L);
		subscription.setEmail("test@abc.com");
		subscription.setFirstName("test");
		subscription.setGender(Gender.male);
		subscription.setNewsletterId(2);
		subscription.setDateOfBirth(LocalDate.parse("12/01/1985", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		subscription.setId(1L);
		response.setSubscriptions(Arrays.asList(subscription));
		when(subscriptionService.createSubscription(any(Subscription.class))).thenReturn(response);
		when(subscriptionService.getAllSubscriptions()).thenReturn(response);
		doNothing().when(subscriptionService).cancelSubscription(1L);
	}

	@Test
	void createSubscription_valid() throws Exception {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setFirstName("test");
		subscriptionRequest.setEmail("test@abc.com");
		subscriptionRequest.setGender(Gender.male);
		subscriptionRequest.setNewsletterId(2);
		subscriptionRequest.setDateOfBirth(dob);
		subscriptionRequest.setConsentFlag(Boolean.TRUE);
		String newSubscription = "{\"email\":\"test@abc.com\",\"firstName\":\"test\",\"gender\":\"female\",\"newsletterId\":2,\"consentFlag\":true,\"dateOfBirth\":\"12/01/1985\"}";
		MvcResult result = mockMvc.perform(post("/subscription/subscribe")
				.contentType(MediaType.APPLICATION_JSON)
				.header("auth-user-id", "1")
				.content(newSubscription))
				.andExpect(status().isCreated())
				.andReturn();
		SubscriptionResponse response = objectMapper.readValue(result.getResponse().getContentAsString(),
				SubscriptionResponse.class);
		assertThat(response.getSubscriptions().get(0).getId(), is(1L));
	}

	@Test
	void createSubscription_invalidJson() throws Exception {
		String newSubscription = "xyz";
		mockMvc.perform(post("/subscription/subscribe")
				.contentType(MediaType.APPLICATION_JSON)
				.header("auth-user-id", "1")
				.content(newSubscription))
				.andExpect(status().isBadRequest());
	}

	private static Stream<Arguments> invalidSubscriptionRequests() {
		String blankEmail = "{\"email\":\"\",\"firstName\":\"test\",\"gender\":\"male\"," +
				"\"newsletterId\":2,\"consentFlag\":true,\"dateOfBirth\":\"12/01/1985\"}";
		String blankDateOfBirth = "{\"email\":\"abc@gmail.com\",\"firstName\":\"test\",\"gender\":\"male\"," +
				"\"newsletterId\":2,\"consentFlag\":true,\"dateOfBirth\":\"\"}";
		String blankNewsletter = "{\"email\":\"abc@gmail.com\",\"firstName\":\"test\",\"gender\":\"female\"," +
				"\"newsletterId\":\"\",\"consentFlag\":true,\"dateOfBirth\":\"12/01/1985\"}";
		return Stream.of(
				Arguments.of(blankEmail, "email", "Email is required"),
				Arguments.of(blankDateOfBirth, "dateOfBirth", "Date of Birth is required"),
				Arguments.of(blankNewsletter, "newsletterId", "Newsletter Id Is required")
		);
	}

	@ParameterizedTest
	@MethodSource("invalidSubscriptionRequests")
	void createSubscription_invalidRequests(String request, String fieldExpression, String message) throws Exception {
		MvcResult result = mockMvc.perform(post("/subscription/subscribe")
				.contentType(MediaType.APPLICATION_JSON)
				.header("auth-user-id", "1")
				.content(request))
				.andExpect(status().isBadRequest())
				.andReturn();
		ApiError error = objectMapper.readValue(result.getResponse().getContentAsString(), ApiError.class);
		assertThat(error.getValidationErrors().size(), is(1));
		assertTrue(error.getValidationErrors().containsKey(fieldExpression));
		assertThat(error.getValidationErrors().get(fieldExpression), is(message));
	}

	@Test
	void cancelSubscription_valid() throws Exception {
		mockMvc.perform(delete("/subscription/cancel_subscription/1")
				.contentType(MediaType.APPLICATION_JSON)
				.header("auth-user-id", "1"))
				.andExpect(status().isNoContent());
	}

	@Test
	void getSubscription_valid() throws Exception {
		SubscriptionResponse response = new SubscriptionResponse();
		Subscription subscription = new Subscription();
		subscription.setId(1L);
		subscription.setEmail("test@abc.com");
		subscription.setFirstName("test");
		subscription.setGender(Gender.male);
		subscription.setNewsletterId(2);
		subscription.setDateOfBirth(LocalDate.parse("12/01/1985", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		subscription.setId(1L);
		response.setSubscriptions(Arrays.asList(subscription));
		when(subscriptionService.getSubscription(any(Subscription.class))).thenReturn(response);
		String newSubscription = "{\"email\":\"test@abc.com\",\"firstName\":\"test\",\"gender\":\"female\",\"newsletterId\":2,\"consentFlag\":true,\"dateOfBirth\":\"12/01/1985\"}";
		mockMvc.perform(get("/subscription/get_subscription")
				.contentType(MediaType.APPLICATION_JSON)
				.header("auth-user-id", "1")
				.content(newSubscription))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.subscriptions[0].id", notNullValue()))
				.andExpect(jsonPath("$.subscriptions[0].id", equalTo(1)));
	}

	@Test
	void getSubscription_notFound() throws Exception {
		when(subscriptionService.getSubscription(any(Subscription.class))).thenThrow(new SubscriptionNotFoundException());
		String newSubscription = "{\"email\":\"test@abc.com\",\"firstName\":\"test\",\"gender\":\"female\",\"newsletterId\":2,\"consentFlag\":true,\"dateOfBirth\":\"12/01/1985\"}";
		mockMvc.perform(get("/subscription/get_subscription")
				.contentType(MediaType.APPLICATION_JSON)
				.header("auth-user-id", "1")
				.content(newSubscription))
				.andExpect(status().isNotFound());
	}

	@Test
	void getAllSubcriptions() throws Exception {
		String newSubscription = "{\"email\":\"test@abc.com\",\"firstName\":\"test\",\"gender\":\"female\",\"newsletterId\":2,\"consentFlag\":true,\"dateOfBirth\":\"12/01/1985\"}";
		mockMvc.perform(get("/subscription/get_all_subscriptions")
				.contentType(MediaType.APPLICATION_JSON)
				.header("auth-user-id", "1")
				.content(newSubscription))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.subscriptions[0].id", notNullValue()))
				.andExpect(jsonPath("$.subscriptions[0].id", equalTo(1)));
	}

}
