package com.adidas.subscription.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
public class ApiError {
    private HttpStatus status;
    private LocalDateTime timestamp;
    private String error;
    private String debugMessage;
    Map<String, String> validationErrors = new HashMap<>();

    public ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus status, String error,  Throwable ex, Map<String,String> validationErrors) {
        this();
        this.status = status;
        this.error = error;
        this.validationErrors = validationErrors;
    }

    public ApiError(HttpStatus status, String error, Throwable ex) {
        this();
        this.status = status;
        this.error = error;
        this.debugMessage = ex.getLocalizedMessage();
    }
}
