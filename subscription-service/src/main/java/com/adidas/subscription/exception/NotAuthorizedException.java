package com.adidas.subscription.exception;

public class NotAuthorizedException extends RuntimeException{
    public NotAuthorizedException() {
        super("auth-user-id header not found");
    }
}
