package com.adidas.subscription.exception;

public class SubscriptionServiceException extends RuntimeException{
    public SubscriptionServiceException(Throwable e){
        super("Service exception occurred", e);
    }
}
