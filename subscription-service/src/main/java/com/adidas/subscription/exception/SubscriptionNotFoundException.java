package com.adidas.subscription.exception;

public class SubscriptionNotFoundException extends RuntimeException{
    public SubscriptionNotFoundException(){
        super("Subscription not found!");
    }

    public SubscriptionNotFoundException(Throwable ex){
        super("Subscription not found!", ex);
    }
}
