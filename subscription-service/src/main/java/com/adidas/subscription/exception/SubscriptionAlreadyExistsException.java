package com.adidas.subscription.exception;

public class SubscriptionAlreadyExistsException extends RuntimeException {
    public SubscriptionAlreadyExistsException(Throwable e){
        super("Subscription already exists", e);
    }
}
