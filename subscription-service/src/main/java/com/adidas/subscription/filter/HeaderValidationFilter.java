package com.adidas.subscription.filter;

import com.adidas.subscription.exception.NotAuthorizedException;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static com.adidas.subscription.constants.SubscriptionConstants.REQUEST_PATH_SUBSCRIBE;

@Component
public class HeaderValidationFilter implements Filter {


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        if(httpRequest.getServletPath().contains(REQUEST_PATH_SUBSCRIBE)) {
            String headerUserId = httpRequest.getHeader("auth-user-id");
            if(headerUserId == null || headerUserId.isEmpty()){
                throw new NotAuthorizedException();
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
