package com.adidas.subscription.service;

import com.adidas.subscription.model.Subscription;
import com.adidas.subscription.model.SubscriptionResponse;
import com.adidas.subscription.repository.SubscriptionRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class InvokeEmailService {

    private final String emailServiceUrl;
    private final RestTemplate restTemplate;
    private final KafkaTemplate<String,String> kafkaTemplate;

    public InvokeEmailService(@Value("${api.email-service.url}") String emailServiceUrl,
                               RestTemplate restTemplate, KafkaTemplate<String,String> kafkaTemplate) {
        this.emailServiceUrl = emailServiceUrl;
        this.restTemplate = restTemplate;
        this.kafkaTemplate = kafkaTemplate;
    }
    @CircuitBreaker(name = "emailService", fallbackMethod = "fallbackEmailService")
    public String invoke() {
        kafkaTemplate.send("test", "email");
        return "Email sent successfully";
        //return restTemplate.postForObject(emailServiceUrl, null, String.class);
    }

    public String fallbackEmailService(Exception e) {

        return "Email service is down. Email will be sent later";
    }
}
