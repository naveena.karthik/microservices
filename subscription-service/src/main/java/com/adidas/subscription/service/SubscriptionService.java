package com.adidas.subscription.service;

import com.adidas.subscription.exception.SubscriptionAlreadyExistsException;
import com.adidas.subscription.exception.SubscriptionNotFoundException;
import com.adidas.subscription.exception.SubscriptionServiceException;
import com.adidas.subscription.model.Subscription;
import com.adidas.subscription.model.SubscriptionResponse;
import com.adidas.subscription.repository.SubscriptionRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static com.adidas.subscription.constants.SubscriptionConstants.UNIQUE_VIOLATION_ERROR_CODE;

@Service
public class SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final InvokeEmailService invokeEmailService;

    public SubscriptionService(SubscriptionRepository subscriptionRepository,
                               InvokeEmailService invokeEmailService) {
        this.subscriptionRepository = subscriptionRepository;
        this.invokeEmailService = invokeEmailService;
    }

    public SubscriptionResponse createSubscription(Subscription subscriptionRequest) {

        SubscriptionResponse response = new SubscriptionResponse();

        try {
            Subscription subscription = subscriptionRepository.save(subscriptionRequest);
            response.setSubscriptions(Arrays.asList(subscription));
            // Invoke sendEmail rest API
            String emailResponse = invokeEmailService.invoke();

            response.setEmailResponse(emailResponse);

        } catch (DataIntegrityViolationException e) {

            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {

                if (PSQLException.class.equals(t.getClass())) {
                    PSQLException postgresException = (PSQLException) t;

                    if (UNIQUE_VIOLATION_ERROR_CODE.equals(postgresException.getSQLState())) {
                        throw new SubscriptionAlreadyExistsException(e);
                    }
                }
            }
        } catch (Exception ex){
            throw new SubscriptionServiceException(ex);
        }

        return response;
    }

    @Transactional
    public void cancelSubscription(Long id) {

        try {
            subscriptionRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex){
            throw new SubscriptionNotFoundException(ex);
        }
        catch (Exception ex){
            throw new SubscriptionServiceException(ex);
        }


    }

    public SubscriptionResponse getSubscription(Subscription subscriptionRequest) {

        try {
            Subscription subscription = subscriptionRepository.findByEmailAndNewsletterIdAndDateOfBirth
                    (subscriptionRequest.getEmail(), subscriptionRequest.getNewsletterId(),
                            subscriptionRequest.getDateOfBirth()).orElseThrow(SubscriptionNotFoundException::new);
            SubscriptionResponse response = new SubscriptionResponse();
            response.setSubscriptions(Arrays.asList(subscription));
            return response;
        } catch (SubscriptionNotFoundException ex){
            throw ex;
        } catch (Exception ex){
            throw new SubscriptionServiceException(ex);
        }

    }

    public SubscriptionResponse getAllSubscriptions() {
        try {
            SubscriptionResponse response = new SubscriptionResponse();
            response.setSubscriptions(subscriptionRepository.findAll());
            return response;
        } catch (Exception ex){
            throw new SubscriptionServiceException(ex);
        }

    }
}
