package com.adidas.subscription.constants;

public class SubscriptionConstants {
    public static final String MISSING_DOB = "Date of Birth is required";
    public static final String MISSING_EMAIL = "Email is required";
    public static final String MISSING_CONSENT = "Consent Flag Is required";
    public static final String MISSING_NEWSLETTER_ID = "Newsletter Id Is required";
    public static final String UNIQUE_VIOLATION_ERROR_CODE = "23505";
    public static final String REQUEST_PATH_SUBSCRIBE = "/subscription/subscribe";
}
