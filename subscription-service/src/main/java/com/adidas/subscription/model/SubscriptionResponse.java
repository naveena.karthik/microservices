package com.adidas.subscription.model;

import lombok.Data;

import java.util.List;

@Data
public class SubscriptionResponse {
    List<Subscription> subscriptions;
    String emailResponse;
}
