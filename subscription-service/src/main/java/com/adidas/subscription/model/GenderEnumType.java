package com.adidas.subscription.model;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

public class GenderEnumType extends org.hibernate.type.EnumType {
    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
            throws HibernateException, SQLException {

        if (Objects.nonNull(rs.getString(names[0]))) {
            return Gender.fromName(rs.getString(names[0]));
        }

        return null;

    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
            throws SQLException {
        st.setObject(index, Objects.nonNull(value) ? value.toString() : null, Types.OTHER);
    }
}
