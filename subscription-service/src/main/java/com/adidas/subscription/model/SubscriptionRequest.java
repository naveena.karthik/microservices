package com.adidas.subscription.model;

import com.adidas.subscription.constants.SubscriptionConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class SubscriptionRequest {

    String firstName;
    Gender gender;

    @NotEmpty(message = SubscriptionConstants.MISSING_EMAIL)
    String email;
    @NotNull(message = SubscriptionConstants.MISSING_DOB)
    @JsonFormat(pattern="dd/MM/yyyy")
    LocalDate dateOfBirth;
    @NotNull(message = SubscriptionConstants.MISSING_NEWSLETTER_ID)
    Integer newsletterId;
    @NotNull(message = SubscriptionConstants.MISSING_CONSENT)
    boolean consentFlag;
}
