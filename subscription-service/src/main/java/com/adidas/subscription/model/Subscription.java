package com.adidas.subscription.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@Data
@Entity
@Table(name= "subscription")
@TypeDef(
        name = "gender_enum_type",
        typeClass = GenderEnumType.class
)
public class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    private String email;
    @Column
    private String firstName;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "gender")
    @Type( type = "gender_enum_type" )
    private Gender gender;
    @Column
    private LocalDate dateOfBirth;
    @Column
    private Integer newsletterId;
    @Column
    private boolean consentFlag;

    public Subscription(SubscriptionRequest request){
        this.firstName = request.getFirstName();
        this.gender = request.getGender();
        this.email = request.getEmail();
        this.consentFlag = request.isConsentFlag();
        this.dateOfBirth = request.getDateOfBirth();
        this.newsletterId = request.getNewsletterId();
    }

}
