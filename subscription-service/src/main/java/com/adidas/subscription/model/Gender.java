package com.adidas.subscription.model;

public enum Gender {

    male("male"),
    female("female"),
    other("other");

    private final String name;

    private Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString(){
        return this.name;
    }

    public static Gender fromName(String name) {
        switch (name) {
            case "male":
                return Gender.male;

            case "female":
                return Gender.female;

            case "other":
                return Gender.other;

            default:
                throw new IllegalArgumentException("Gender name [" + name + "] not supported.");
        }
    }
}
