package com.adidas.subscription.repository;

import com.adidas.subscription.model.Subscription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {

    void deleteByEmailAndNewsletterIdAndDateOfBirth(
            @Param("id") String email,
            @Param("newsletterId") Integer newsletterId,
            @Param("dateOfBirth") LocalDate dateOfBirth);

    Optional<Subscription> findByEmailAndNewsletterIdAndDateOfBirth(
            @Param("id") String email,
            @Param("newsletterId") Integer newsletterId,
            @Param("dateOfBirth") LocalDate dateOfBirth);

    List<Subscription> findAll();
}
