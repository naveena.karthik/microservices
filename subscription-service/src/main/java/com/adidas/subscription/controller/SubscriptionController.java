package com.adidas.subscription.controller;

import com.adidas.subscription.model.Subscription;
import com.adidas.subscription.model.SubscriptionRequest;
import com.adidas.subscription.model.SubscriptionResponse;
import com.adidas.subscription.service.SubscriptionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/subscription")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @RequestMapping(value = "/subscribe", method = RequestMethod.POST)
    public ResponseEntity<SubscriptionResponse> createSubscription(@Valid @RequestBody SubscriptionRequest subscriptionRequest) {
        Subscription subscription = new Subscription(subscriptionRequest);
        SubscriptionResponse response = subscriptionService.createSubscription(subscription);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @RequestMapping(value = "/cancel_subscription/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> cancelSubscription(@PathVariable Long id) {
        subscriptionService.cancelSubscription(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/get_subscription", method = RequestMethod.GET)
    public SubscriptionResponse getSubscription(@Valid @RequestBody SubscriptionRequest subscriptionRequest) {
        Subscription subscription = new Subscription(subscriptionRequest);
        return subscriptionService.getSubscription(subscription);
    }

    @RequestMapping(value = "/get_all_subscriptions", method = RequestMethod.GET)
    public SubscriptionResponse getAllSubscriptions() {
        return subscriptionService.getAllSubscriptions();
    }
}
