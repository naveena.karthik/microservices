CREATE DATABASE adidas
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
GRANT ALL PRIVILEGES ON DATABASE adidas TO postgres;
\c adidas

CREATE TYPE gender AS ENUM (
    'male',
    'female',
    'other'
);
CREATE SEQUENCE hibernate_sequence START 1;
CREATE TABLE subscription (
    id integer PRIMARY KEY,
    consent_flag boolean NOT NULL,
    date_of_birth date NOT NULL,
    first_name character varying(255),
    email character varying(512) NOT NULL UNIQUE,
    gender gender,
    newsletter_id integer NOT NULL
);
CREATE TABLE token (
    id integer,
    name character varying(255),
	token character varying(512)
);

insert into token (id, name, token)
values (60055, 'naveena', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJuYXZlZW5hIiwiaWF0IjoxNTE2MjM5MDIyfQ.zTynOMWk3iiVGVP6vH3ypXTzs7mk1E8sbr2jFQRv4EY'); 


