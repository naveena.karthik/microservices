package com.adidas.gateway.filter;

import com.adidas.gateway.exception.InvalidAuthorizationCredentialsException;
import com.adidas.gateway.model.Token;
import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class AuthFilter extends AbstractGatewayFilterFactory<AuthFilter.Config> {

    private final WebClient.Builder webClientBuilder;
    private final String authServiceUrl;

    @Autowired
    public AuthFilter(WebClient.Builder webClientBuilder,
                      @Value("${api.auth-service.url}") String authServiceUrl) {
        super(Config.class);
        this.webClientBuilder = webClientBuilder;
        this.authServiceUrl = authServiceUrl;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return ((exchange, chain) -> {
            System.out.println("Inside filter");
            if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)){
                throw new InvalidAuthorizationCredentialsException();
            }
            String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
            String[] parts = authHeader.split(" ");
            if(parts.length!=2 && !"Bearer".equals(parts[0])){
                throw new InvalidAuthorizationCredentialsException();
            }
            return webClientBuilder.build()
                    .post()
                    .uri(authServiceUrl + parts[1])
                    .retrieve()
                    .bodyToMono(Token.class)
                    .map(token -> {
                        exchange.getRequest().mutate()
                                .header("auth-user-id", String.valueOf(token.getId()));
                        return exchange;
                    }).flatMap(chain::filter);
        });
    }

    public static class Config {

    }
}
