package com.adidas.gateway.model;

import lombok.Data;

@Data
public class Token {
    private Long id;
    private String token;
    private String name;
}
