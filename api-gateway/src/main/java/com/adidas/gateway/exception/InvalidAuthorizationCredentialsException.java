package com.adidas.gateway.exception;

public class InvalidAuthorizationCredentialsException extends RuntimeException{
    public InvalidAuthorizationCredentialsException() {
        super("Invalid Credentials");
    }
}
