package com.adidas.auth.service.services;

import com.adidas.auth.service.exception.InvalidTokenException;
import com.adidas.auth.service.model.Token;
import com.adidas.auth.service.repository.TokenRepository;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

@Service
public class AuthService {
    @Value("${security.token.secret-key}")
    private String secretKey;
    @Autowired
    private TokenRepository tokenRepository;

    public Token validateToken(String token)  {
        String encodedSecret = Base64.getEncoder().encodeToString(secretKey.getBytes());
        String subject = Jwts.parser()
                .setSigningKey(encodedSecret.getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        Optional<Token> tokenOptional = tokenRepository.findByName(subject);
        if(!tokenOptional.isPresent()){
            throw new InvalidTokenException();
        }
        return tokenOptional.get();
    }
}
