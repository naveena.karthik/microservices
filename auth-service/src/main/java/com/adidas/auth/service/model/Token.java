package com.adidas.auth.service.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@Data
@Entity
@Table(name= "token")
public class Token {
    @Id
    private Long id;
    private String token;
    private String name;
}
