package com.adidas.auth.service.repository;

import com.adidas.auth.service.model.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends CrudRepository<Token, Long> {

    Optional<Token> findByName(
            @Param("name") String name);

}
