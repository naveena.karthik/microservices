package com.adidas.auth.service.exception;

public class InvalidTokenException extends RuntimeException{
    public InvalidTokenException() {
        super("Invalid token");
    }
}
