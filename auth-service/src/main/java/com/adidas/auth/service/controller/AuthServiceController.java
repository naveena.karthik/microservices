package com.adidas.auth.service.controller;

import com.adidas.auth.service.model.Token;
import com.adidas.auth.service.services.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AuthServiceController {

    private final AuthService authService;

    public AuthServiceController(AuthService authService) {
        this.authService = authService;
    }

    @RequestMapping(value = "/validateToken", method = RequestMethod.POST)
    public ResponseEntity<Token> validateToken(String token) {
        Token response = authService.validateToken(token);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
}
